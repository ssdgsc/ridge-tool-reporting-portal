﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Reflection;
using System.Collections;


public class MailMaster
{
    private string host = "";
    private MailAddress from = new MailAddress("RidgeToolReportingPortal@Emerson.com", "MVC Template");

    public MailMaster(String mailHost)
    {
        host = mailHost;
    }

    public void sendMailNotification(String to, String cc, String subject, String content)
    {
        MailMessage email = new MailMessage();

        email.IsBodyHtml = true;
        email.To.Add(new MailAddress(to));

        email.From = from;
        email.Subject = subject;
        email.Body = content;
        SmtpClient client = new SmtpClient();
        client.Timeout = 2000000;
        client.Host = host;
        client.Port = 25;
        client.Send(email);
    }

    public void sendMailNotification(String to, String cc, String subject, String body, Attachment attachment)
    {
        MailMessage email = new MailMessage();
        email.IsBodyHtml = true;
        email.To.Add(new MailAddress(to));

        email.From = from;
        email.Subject = subject;
        email.Body = body;
        email.Attachments.Add(attachment);
        SmtpClient client = new SmtpClient();
        client.Timeout = 2000000;
        client.Host = host;
        client.Port = 25;
        client.Send(email);
    }
}