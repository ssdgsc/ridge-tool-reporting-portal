﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

using System.Security.Principal;
using System.Web.Mvc;

namespace RidgeToolReportingPortalModel
{
    [Table("PositionTemplate", Schema = "dbo")]
    public class Position
    {
        [Key]
        public int PositionID { get; set; }
        public string PositionName { get; set; }
    }
}
