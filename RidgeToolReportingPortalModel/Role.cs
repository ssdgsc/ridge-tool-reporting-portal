﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Principal;
using System.Web.Mvc;

namespace RidgeToolReportingPortalModel
{
    [Table("RoleTemplate", Schema = "dbo")]
    public class Role
    {
        [Key]
        public int RoleID { get; set; }
        public String RoleName { get; set; }
    }
}
