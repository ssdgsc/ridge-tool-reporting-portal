﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Principal;
using System.Web.Mvc;

namespace RidgeToolReportingPortalModel
{
    [Table("UserTemplate", Schema = "dbo")]
    public class User
    {
        [Key]
        public int UserID { get; set; }
        public String Username { get; set; }
        public int RoleID { get; set; }
        public int Status { get; set; }
        public int EmployeeID { get; set; }
        public String EmailAddress { get; set; }
        public String CreatedBy { get; set; }
        public DateTime? Created { get; set; }
        public String UpdatedBy { get; set; }
        public DateTime? Updated { get; set; }

        [NotMapped]
        public IEnumerable<SelectListItem> RoleList { get; set; }
        [NotMapped]
        public IEnumerable<SelectListItem> StatusList { get; set; }
        [NotMapped]
        public IEnumerable<SelectListItem> EmployeeList { get; set; }

        [ForeignKey("RoleID")]
        public virtual Role Role { get; set; }
        [ForeignKey("EmployeeID")]
        public virtual Employee employee { get; set; }
    }

    interface CustomIPrincipal : IPrincipal
    {
        int UserId { get; set; }
        string UserName { get; set; }
        string FullName { get; set; }
        string PhotoPath { get; set; }
    }

    public class Custom2IPrincipal : CustomIPrincipal
    {
        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role) { return false; }

        public Custom2IPrincipal(String UserId)
        {
            this.Identity = new GenericIdentity(UserId);
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string PhotoPath { get; set; }
        public string UserKey { get; set; }
        public bool isAdmin { get; set; }
        public long RoleId { get; set; }
    }

    public class CustomPrincipalSerializeModel
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string PhotoPath { get; set; }
        public string UserKey { get; set; }
        public bool isAdmin { get; set; }
        public long RoleId { get; set; }
        public int UserType { get; set; }
    }
    public class Login
    {
        public String Username { get; set; }
        public String Password { get; set; }
        public bool RememberMe { get; set; }
        public bool IsSuccess { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class UserModel
    {
        public int UserID { get; set; }
        public String Username { get; set; }
        public String RoleName{ get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String EmailAddress { get; set; }
        public String CreatedBy { get; set; }
        public DateTime? Created { get; set; }
        public String UpdatedBy { get; set; }
        public DateTime? Updated { get; set; }
        public int Status { get; set; }
    }
}
