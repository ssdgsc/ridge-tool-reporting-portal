﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace RidgeToolReportingPortalModel
{
    [Table("EmployeeTemplate", Schema = "dbo")]
   public class Employee
    {
        [Key]
        public int EmployeeID { get; set; }
        public String Firstname { get; set; }
        public String Middlename { get; set; }
        public String Lastname { get; set; }
        public DateTime DateHired { get; set; }
        public bool IsResigned { get; set; }
        public int SupervisorID { get; set; }
        public int PositionID { get; set; }
        public String CreatedBy { get; set; }
        public DateTime? Created { get; set; }
        public String UpdatedBy { get; set; }
        public DateTime? Updated { get; set; }


        [NotMapped]
        public virtual String FullName
        {
            get
            {
                return Firstname + " " + Middlename + " " + Lastname;
            }
        }

        [NotMapped]
        public IEnumerable<SelectListItem> PositionList { get; set; }
        [NotMapped]
        public IEnumerable<SelectListItem> SupervisorList { get; set; }

        [ForeignKey("PositionID")]
        public virtual Position Position { get; set; }
        [ForeignKey("SupervisorID")]
        public Employee Supervisor { get; set; }

    }
}