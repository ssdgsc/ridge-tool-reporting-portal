﻿$(document).ready(function () {
    var UploadTask = {
        init: function () {
            var self = this;
            self.declaration();
            self.setEvents();
        },

        declaration: function () {
            var self = this;

            self.inputUpload = $("#fileUpload");
            self.btnUpload = $("a.btn-download");

        },

        setEvents: function () {
            var self = this;
            self.btnUpload.off("click").on("click", function (e) {
                self.inputUpload.trigger("click");
            });

            self.inputUpload.on("change", function (e) {
                var $file = this,
                $formData = new FormData();

                if ($file.files.length > 0) {
                    for (var i = 0; i < $file.files.length; i++) {
                        //$formData.append('file-' + i, $file.files[i]);
                        $formData.append($file.files[i].name, $file.files[i]);
                    }
                }

                $.ajax({
                    url: '/Excel/ReadExcel',
                    type: 'POST',
                    data: $formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        var x = data;
                    },
                    error: function (error) {
                        var x = error;
                    }
                });
            });
        },
    };

    var InitUploadTask = function () {
        var uploadTask = Object.create(UploadTask);
        uploadTask.init();
    }

    InitUploadTask();
});