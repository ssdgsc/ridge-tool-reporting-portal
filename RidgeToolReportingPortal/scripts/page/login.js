﻿
$(document).ready(function () {
    var LoginAndRegistrationObjTask = {
        init: function () {
            var self = this;

            self.declaration();
            self.setEvent();
            self.validationHide();

        },
        declaration: function(){
            var self = this;

            self.$btnLogin =  $("#btnLogin");
            self.validationFailDIV = $('#validationFail');
            self.validationIncomplete = $('#validateIncomplete');
            self.genericValidation = $('#validateGeneric');
            self.username = $('#Username');
            self.password = $("#Password");
            self.frmLogin = $("#frmLogin");
            self.firsname = $("#FirstName");
            self.lastname = $('#LastName');
            self.emailaddress = $('#EmailAddress');
            

        },
        setEvent: function(){
            var self = this;
           
            self.username.on('keydown', function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    self.$btnLogin.click();
                }
            });

            self.password.on('keydown', function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    self.$btnLogin.click();
                }
            });

            self.$btnLogin.click(function () {
                $(this).html("Signin in... <i class=\"fa fa-spinner fa-spin fa-fw\"></i>");
                if (!self.isLoginFieldsValid()) {
                    self.validationShow();
                }
                else {
                    self.validationHide();
                    self.processLogin();
                }
            });

                
        },
        validationShow: function () {
            var self = this;

            self.validationFailDIV.show();
            self.validationIncomplete.show();
        },
        validationGenericText: function (message) {
            var self = this;

            self.genericValidation.html(message);
            self.validationFailDIV.show();
        },
        validationHide: function () {
            var self = this;

            self.validationFailDIV.hide();
            self.validationIncomplete.hide();

        },
        isLoginFieldsValid: function(){
            var self = this;

            var valid = true;
            if (self.username.val() === null || self.username.val() === "") {
                valid = false;
                self.username.focus();
            }

            if (self.password.val() === null || self.password.val() === "") {
                valid = false;
                self.password.focus();
            }
            return valid;

        },
        clearRegistrationFields: function(){
            $('#Username').val("");
            $('#FirstName').val("");
            $('#LastName').val("");
            $('#EmailAddress').val("");
        },
        processLogin: function () {
            var self = this;
            $.ajax({
                url: "/Login/Login/",
                type: "POST",
                data: self.frmLogin.serialize(),
                dataType: "json",
                traditional: true,
                success: function (data) {
                    if (!data.IsSuccess) {
                        self.$btnLogin.prop("disabled", false);
                        self.validationGenericText(data.ErrorMessage);
                        setTimeout(function () {
                            self.$btnLogin.html("Sign in");
                        }, 500);

                    } else {
                        window.location = window.landingPageURL;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    self.$btnLogin.prop("disabled", false);
                    self.validationGenericText(errorThrown);
                    setTimeout(function () {
                        self.$btnLogin.html("Sign in");
                    }, 500);
                }
            });
        }

  
    }

    var InitializedLoginAndRegistrayionObjTask = function () {
        var initLoginAndRegistrationObjTask = Object.create(LoginAndRegistrationObjTask);
        initLoginAndRegistrationObjTask.init();
    }
    InitializedLoginAndRegistrayionObjTask();
});



//function showErrorDiv(errorMessage) {
//	$('#validationFail').show();
//	$('#validateGeneric').html(errorMessage);
//	$('#validateGeneric').show();
//}

//function loadRegistration() {
//	$.ajax({
//		url: registerURL,
//		success: function (data) {
//			$('#pageContent').html(data);
//			$('#validationFail').hide();
//			$('#validateIncomplete').hide();
//			document.title = "Sign Up - Computer Engineering Database System";
//		},
//		error: function () {
//		}
//	});
//}

//function checkInput() {
//    $("#btnSignUp").html("Registering Account... <i class=\"fa fa-spinner fa-spin fa-fw\"></i>");

//	$("#btnSignUp").prop("disabled", true);
//	var valid = true;
//	var signupForm = $("#frmSignUp").serialize();

//	$.ajax({
//		url: saveURL,
//		type: "POST",
//		data: signupForm,
//		dataType: "json",
//		traditional: true,
//		success: function (data) {
//			if (data.Data === '1') {
//				$("#btnSignUp").html("Register");
//				$("#btnSignUp").prop("disabled", false);
//				validationAnimation("#validationContainer1", "", "success", "Registration Successful!");
//				clearRegistrationFields();
//				//showErrorDiv(data.ErrorMessage);
//			}
//			else if (data.Data === '2') {
//				$("#btnSignUp").html("Register");
//				$("#btnSignUp").prop("disabled", false);
//				validationAnimation("#validationContainer1", "", "success", "Registration successful but email notification to user failed.");
//                clearRegistrationFields();
//			}
//			else if (data.Data === '3') {
//				$("#btnSignUp").html("Register");
//				$("#btnSignUp").prop("disabled", false);
//				validationAnimation("#validationContainer1", "", "success", "Registration successful but email notification to admin/s failed.");
//                clearRegistrationFields();
//			}
//			else {
//				$("#btnSignUp").prop("disabled", false);
//				//window.location = window.landingPageURL;
//                validationAnimation("#validationContainer1", "", "fail", JSON.stringify(data.Data));
//                // Remove Button animation when validation fail
//                setTimeout(function () {
//                    $("#btnSignUp").html("Register");
//                }, 500);
//			}
//		},
//		error: function (jqXHR, textStatus, errorThrown) {
//            $("#btnSignUp").prop("disabled", false);
//            initReusableModal("Error! ", errorThrown, false, false, undefined, undefined, undefined);
//		}
//	});
//}

//function clearRegistrationFields() {
//	$('#Username').val("");
//	$('#FirstName').val("");
//	$('#LastName').val("");
//	$('#EmailAddress').val("");
//}