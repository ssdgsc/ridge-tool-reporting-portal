﻿$(document).ready(function () {
    var UserObjTask = {
        init: function () {
            var self = this;
            self.declaration();
            self.setEvents();
            self.populateUserDataTable();
        },

        declaration: function () {
            var self = this;

            self.$btnAdd = $("#btnAdd");
            self.$userTable = $("#userTable");
            self.$btnSave = $("#btnSave");
            self.$frmUser = $("#frmUser");
            self.$btnDeleteUser = $("#btnDeleteUser");
            self.$validationMessage = $("#validationMessage");
            self.$empStatus = $("#empStatus");

        },

        deleteUserData: function (id) {
            var self = this;

            deleteData('/User/Delete/' + id);
            self.populateUserDataTable();
        },

        clearValidationUserForm: function () {
            var self = this;

            self.$validationMessage.text("");
            self.$validationMessage.removeClass("text-danger text-info");
        },

        refreshUserModalForm: function () {
            var self = this;

            $("#cboEmployee").val("");
            $("#Username").val("");
            $("#EmailAddress").val("");
            $("#cboRole").val("");
            $("#cboStatus").val("");

            self.populateUserDataTable();
        },

        editUserData: function (id) {

            renderingData('/User/AddEditUser/' + id);
            $('#exampleModal').modal('show');
        },

        setEvents: function () {
            var self = this;

            self.$btnAdd.on('click', function () {
                self.clearValidationUserForm();
                renderingData('/User/AddEditUser/0');
            });
            
            self.$empStatus.on('change', function () {
                self.populateUserDataTable();
            });

            self.$btnSave.click(function () {
                self.clearValidationUserForm();
                $.ajax({
                    url: "/User/Post",
                    type: "POST",
                    data: $("#frmUser").serialize(),
                    dataType: 'json',
                    success: function (data) {
                        if (data.Data === '1') {

                            self.$validationMessage.text("Record successfully saved");
                            self.$validationMessage.addClass("text-info");
                            self.refreshUserModalForm();
                        }
                        else {
                            self.$validationMessage.append(data.Data);
                            self.$validationMessage.addClass("text-danger");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console(data);
                    }
                });
            });

            $('#userTable tbody').on('click', 'button', function () {
                var buttonValue = $(this).val();
                if (buttonValue.indexOf("editUser") > -1) {
                    self.clearValidationUserForm();
                    self.editUserData(buttonValue.replace('editUser', ''));
                }
                else {
                    alert("test");
                    self.deleteUserData(buttonValue.replace('deleteUser', ''));
                }
            });
        },

        populateUserDataTable: function () {
            var self = this;

            self.$userTable.dataTable({
                "destroy": true,
                "stripeClasses": [],
                "ajax":{
                    "url": "/User/AjaxList",
                    "data": {
                        status: self.$empStatus.val()
                    }
                },
                "pagingType": "full_numbers",
                "searching": true,
                "oLanguage": {
                    "sLengthMenu": " _MENU_ ",
                    "sInfo": "_START_ to _END_ of _TOTAL_ records",
                    "sProcessing": "<img src='/Theme/img/loading.gif'><br>Loading...",
                    "sInfoEmpty": "",
                    "sInfoFiltered": ""
                },
                "aoColumnDefs": [
                    {
                        "aTargets": [0],
                        "bSortable": true,
                        "bSearchable": true
                    },
                    {
                        "aTargets": [1],
                        "bSortable": true,
                        "bSearchable": true
                    },
                    {
                        "aTargets": [2],
                        "bSortable": true,
                        "bSearchable": true
                    },
                    {
                        "aTargets": [3],
                        "bSortable": false,
                        "bSearchable": false,
                        "render": function (data, type, aData, meta) {
                            return '<button type="button" value="editUser' + aData[3] + '" class="btn btn-primary tblButton"><i class="fa fa-pencil-square-o btnIcon"></i></button> &nbsp'
                                                        + '<button type="button" value="deleteUser' + aData[3] + '" class="btn btn-danger tblButton"><i class="fa fa-trash-o btnIcon" aria-hidden="true"></i></button>';
                        }

                    }
                ]
            });
        }

    }

    var InitializedUserObjTask = function () {
        var userObjTask = Object.create(UserObjTask);
        userObjTask.init();
    }

    InitializedUserObjTask();
});
