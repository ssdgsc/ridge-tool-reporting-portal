﻿$(document).ready(function () {
    var EmployeeObjTask = {
        init: function () {
            var self = this;

            self.declaration();
            self.setEvents();
            self.populateEmployeeDataTable();
        },
        declaration: function () {
            var self = this;

            self.$employeeTable = $("#employeeTable");
            self.$btnAdd = $("#btnAddEmployee");
            self.$btnSave = $("#btnSave");
            self.$frmUser = $("#frmEmployee");
            self.$btnDeleteUser = $("#btnDeleteEmployee");
            self.$validationMessage = $("#validationMessage");
        },
        editEmployeeData: function (id) {
            renderingData('/Employee/AddEditEmployee/' + id);
            $('#exampleModal').modal('show');
        },
        deleteEmployeeData: function(id){
            var self = this;

            deleteData('/Employee/Delete/' + id);
            self.populateEmployeeDataTable();
        },
        clearValidationEmployeeForm: function () {
            var self = this;

            self.$validationMessage.text("");
            self.$validationMessage.removeClass("text-danger text-info");
        },
        clearValidationUserForm: function () {
            var self = this;

            self.$validationMessage.text("");
            self.$validationMessage.removeClass("text-danger text-info");
        },
        setEvents: function () {
            var self = this;

            self.$btnAdd.on('click', function () {
                self.clearValidationEmployeeForm();
                renderingData('/Employee/AddEditEmployee/0');
            });

            self.$btnSave.click(function () {
                self.clearValidationEmployeeForm();
                $.ajax({
                    url: "/Employee/Post",
                    type: "POST",
                    data: $("#frmEmployee").serialize(),
                    dataType: 'json',
                    success: function (data) {
                        if (data.Data === '1') {

                            self.$validationMessage.text("Record successfully saved");
                            self.$validationMessage.addClass("text-info");
                            self.refreshEmployeeModalForm();
                        }
                        else {
                            self.$validationMessage.append(data.Data);
                            self.$validationMessage.addClass("text-danger");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            });

            $('#employeeTable tbody').on('click', 'button', function () {
                var buttonValue = $(this).val();
                if (buttonValue.indexOf("editEmployee") > -1) {
                    self.editEmployeeData(buttonValue.replace('editEmployee', ''));
                }
                else {
                    self.deleteEmployeeData(buttonValue.replace('deleteEmployee', ''));
                }
            });
        },
        refreshEmployeeModalForm: function () {
            var self = this;

            $("#Firstname").val("");
            $("#Lastname").val("");
            $("#Middlename").val("");
            $("#cboPosition").val("");
            $("#cboSupervisor").val("");
            $("#dateHired").val("");

            self.populateEmployeeDataTable();
                      
            
        },
        populateEmployeeDataTable: function () {
            var self = this;
            $(".dataTables_filter").append("<p>Sample</p>");

            self.$employeeTable.DataTable({
                "destroy": true,
                "stripeClasses": [],
                "ajax": "/Employee/AjaxList",
                "pagingType": "full_numbers",
                "searching": true,
                "oLanguage": {
                    "sLengthMenu": " _MENU_ ",
                    "sInfo": "_START_ to _END_ of _TOTAL_ records",
                    "sProcessing": "<img src='/Theme/img/loading.gif'><br>Loading...",
                    "sInfoEmpty": "",
                    "sInfoFiltered": ""
                },
                "aoColumnDefs": [
                    {
                        "aTargets": [0],
                        "bSortable": true,
                        "bSearchable": true
                    },
                    {
                        "aTargets": [1],
                        "bSortable": true,
                        "bSearchable": true
                    },
                    {
                        "aTargets": [2],
                        "bSortable": true,
                        "bSearchable": true
                    },
                    {
                        "aTargets": [3],
                        "bSortable": true,
                        "bSearchable": true
                    },
                    {
                        "aTargets": [4],
                        "bSortable": true,
                        "bSearchable": true
                    },
                    {
                        "aTargets": [5],
                        "bSortable": false,
                        "bSearchable": false,
                        "render": function (data, type, aData, meta) {
                            return '<button type="button" value="editEmployee' + aData[5] + '" class="btn btn-primary tblButton"><i class="fa fa-pencil-square-o btnIcon"></i></button> &nbsp'
                                                     + '<button type="button" value="deleteEmployee' + aData[5] + '" class="btn btn-danger tblButton"><i class="fa fa-trash-o btnIcon" aria-hidden="true"></i></button>';

                        }
                    }
                ]
            });
        }

    }


    var InitializedEmployeeObjTask = function () {
        var employeeObjTask = Object.create(EmployeeObjTask);
        employeeObjTask.init();
    }

    InitializedEmployeeObjTask();
});

