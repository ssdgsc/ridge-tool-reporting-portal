﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Syncfusion.Presentation;
using System.IO;
//using Spire.Presentation;
//using Spire.Presentation.Drawing;
using Syncfusion.Presentation;
//using Office = Microsoft.Office.Core;
//using PowerPoint = Microsoft.Office.Interop.PowerPoint;

namespace RidgeToolReportingPortal.Controllers
{
    public class PPTController : Controller
    {
        // GET: PPT
        public ActionResult Index()
        {
            return View();
        }

        [CustomAuthorize]
        public JsonResult ProcessPPT()
        {
            string successtx = "AH!";
            string path = Server.MapPath("~/Template/proActTemplate.pptx"); 
            bool isReadable = true;
            try
            {
                using (FileStream stream = System.IO.File.Open(path, FileMode.Open, FileAccess.Read))
                {

                    isReadable = true; // <- File has been opened
                }
            }
            catch (IOException)
            {
                isReadable = false; // <- File failed to open
            }

            try
            {

                IPresentation _IPres = Presentation.Open(Server.MapPath("~/Template/ProActTemplate.pptx"));

                ISlide slide = _IPres.Slides[1];
                ISlide clone = slide.Clone();

                string[] imgnames = { "graph1.png", "graph2.jpg" };
                int left = 100;
                int top = 100;
                int ctr = 1;
                //FileStream first_img = null;
                foreach (string imgname in imgnames) {
                    // MULTIPLE IMAGES ON A SINGLE SLIDE
                    FileStream _imageStream = new FileStream((Path.Combine(Server.MapPath("~/Template/"), imgname)), FileMode.Open);
                    IPicture picture = slide.Pictures.AddPicture(_imageStream, left, top, 200, 100);
                    left += 200;
                    _imageStream.Dispose();

                    // DYNAMIC SLIDES DEPENDS ON THE NUMBER OF IMAGES


                    //if (ctr == 1)
                    //{
                    //    first_img = new FileStream((Path.Combine(Server.MapPath("~/Template/"), imgname)), FileMode.Open);

                    //}
                    //else {
                    //    FileStream _imageStream = new FileStream((Path.Combine(Server.MapPath("~/Template/"), imgname)), FileMode.Open);
                    //    clone.Pictures.AddPicture(_imageStream, 100, 100, 200, 100);
                    //    _IPres.Slides.Add(clone);
                    //    _imageStream.Dispose();
                    //}
                    //ctr++;
                }

                //slide.Pictures.AddPicture(first_img, 100, 100, 200, 100);
                _IPres.Save((Path.Combine(Server.MapPath("~/Template/"), "test.pptx")));
                
                _IPres.Close();


                if (System.IO.File.Exists(Path.Combine(Server.MapPath("~/Template/"), "test.pptx")))
                {
                    successtx = "YEAH!!";
                }
                else
                {
                    successtx = "UH-OH!";
                }
            }
            catch (Exception ex)
            {
                successtx = ex.Message.ToString();
            }

            return Json(new { Data = successtx, ContentType = "" }, JsonRequestBehavior.AllowGet);
        }
    }
}