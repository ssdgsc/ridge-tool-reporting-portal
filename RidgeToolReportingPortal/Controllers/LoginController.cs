﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.DirectoryServices;
using RidgeToolReportingPortalDAL;
using RidgeToolReportingPortalModel;

using System.Web.Security;
using System.Web.Script.Serialization;
namespace RidgeToolReportingPortal.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult Index()
        {
            RidgeToolReportingPortalModel.Custom2IPrincipal customUser = User as RidgeToolReportingPortalModel.Custom2IPrincipal;
            if (customUser != null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult SignUp()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SignIn()
        {
            Login loginModel = new Login();

            if (Request.Cookies["Login"] != null)
            {
                loginModel.Username = Request.Cookies["Login"]["usr"].ToString();
                loginModel.Password = Request.Cookies["Login"]["pwd"].ToString();
                loginModel.RememberMe = true;
            }
            return View(loginModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public JsonResult Login(Login loginModel)
        {
            if (ModelState.IsValid)
            {
                string _username = loginModel.Username;
                string _userpassword = loginModel.Password;

                string inputUser = _username;
                string[] sliceUsername = _username.Split("-".ToCharArray());
                _username = sliceUsername[0];
                string[] checkDomain = _username.Split("\\".ToString().ToCharArray());

                if (checkDomain.Length > 1)
                    _username = checkDomain[1].ToString();

                bool isLogged = false;
                string path = "";
                string filterAttributes = "";
                string emailAdd = "";
                string businessUnit = "";
                string location = "";
                string name = "";
                string fullUserName = "EMRSN" + @"\" + _username;
                string emailDisplayName = "";
                DirectoryEntry de = new DirectoryEntry(path, fullUserName, _userpassword);

                try
                {
                    Object obj = de.NativeObject;
                    DirectorySearcher ds = new DirectorySearcher(de);
                    ds.Filter = "(SAMAccountName=" + _username + ")";
                    ds.PropertiesToLoad.Add("cn");
                    ds.PropertiesToLoad.Add("mail");
                    ds.PropertiesToLoad.Add("company");
                    ds.PropertiesToLoad.Add("co");
                    ds.PropertiesToLoad.Add("givenName");
                    ds.PropertiesToLoad.Add("SN");
                    SearchResult result = ds.FindOne();

                    if (result != null)
                    {
                        path = result.Path;
                        filterAttributes = (String)result.Properties["cn"][0];
                        emailAdd = (String)result.Properties["mail"][0];
                        businessUnit = (String)result.Properties["company"][0];
                        location = (String)result.Properties["co"][0];
                        name = (String)result.Properties["givenName"][0] + " " + (String)result.Properties["SN"][0];
                        emailDisplayName = (String)result.Properties["SN"][0] + ", " + (String)result.Properties["givenName"][0];
                        isLogged = true;
                    }

                    if (isLogged == true)
                    {
                        User userDetails = new User();
                        var uDal = new UserDAL(new DBContext());
                        userDetails = uDal.FindUser(_username);

                        if (userDetails.UserID > 0)
                        {
                            //if (userDetails.IsApproved == 1)
                            //{
                                CustomPrincipalSerializeModel serializerUser = new CustomPrincipalSerializeModel();
                                serializerUser.UserID = userDetails.UserID;
                                serializerUser.UserName = userDetails.Username;
                                //serializerUser.isAdmin = userDetails.IsAdmin;

                                JavaScriptSerializer serializer = new JavaScriptSerializer();
                                string userData = serializer.Serialize(serializerUser);

                                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                                    1,
                                    userDetails.Username,
                                    DateTime.Now,
                                    DateTime.Now.AddDays(2),
                                    true,
                                    userData,
                                    FormsAuthentication.FormsCookiePath);

                                string encTicket = FormsAuthentication.Encrypt(authTicket);
                                HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                                faCookie.Expires = authTicket.Expiration;
                                Response.Cookies.Add(faCookie);

                                HttpCookie rmbrCookie = new HttpCookie("Login");

                                if (loginModel.RememberMe)
                                {
                                    rmbrCookie.Values.Add("usr", loginModel.Username);
                                    rmbrCookie.Values.Add("pwd", loginModel.Password);
                                    rmbrCookie.Expires = DateTime.Now.AddDays(15);
                                    Response.Cookies.Add(rmbrCookie);
                                }
                                else
                                {
                                    rmbrCookie.Expires = DateTime.Now.AddDays(-1d);
                                    Response.Cookies.Add(rmbrCookie);
                                }

                                loginModel.IsSuccess = true;
                                loginModel.ErrorMessage = "";
                            //}
                            //else
                            //{
                            //    loginModel.IsSuccess = false;
                            //    loginModel.ErrorMessage = "Your user account is not yet activated. Please contact your system administrator. ";
                            //}
                        }
                        else
                        {
                            loginModel.IsSuccess = false;
                            loginModel.ErrorMessage = "User is not registered in ECDB";
                        }
                    }
                    else
                    {
                        loginModel.IsSuccess = false;
                        loginModel.ErrorMessage = "Cant find user " + _username;
                    }

                }
                catch (Exception ex)
                {
                    loginModel.IsSuccess = false;
                    loginModel.ErrorMessage = "Your Active Directory Account Validation failed. Please try again by re-typing all your credentials. " + ex.Message;
                }
            }

            return Json(loginModel);
        }
    }
}