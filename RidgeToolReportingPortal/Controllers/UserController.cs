﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

using RidgeToolReportingPortalDAL;
using RidgeToolReportingPortalModel;

namespace RidgeToolReportingPortal.Controllers
{
    public class UserController : Controller
    {
        UserDAL uDal = new UserDAL(new DBContext());
        ClsMyContext cls = new ClsMyContext(ConfigurationManager.ConnectionStrings["TemplateDB"].ConnectionString);
        GenerateIEnumerableList genList = null;
        // GET: Users
        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Index2(int i = 1)
        {
            return View(i);
        }

        [CustomAuthorize]
        public ActionResult AjaxList(int? status)
        {
            var allList = uDal.ListUser(status);
            var result = from c in allList.ToList()
                         select new[] {
                             c.employee.FullName,
                             c.EmailAddress,
                             c.Role != null ? c.Role.RoleName : "",
                             c.UserID.ToString(),
                         };
            return Json(new
            {
                aaData = result,
            },
            JsonRequestBehavior.AllowGet);
        }

     
        [CustomAuthorize]
        public ActionResult AddEditUser(int id)
        {
            User model = null;
            if (id != 0)
            {
                model = uDal.FindUser(id);
            }
            if (model == null)
            {
                model = new User(); 
            }
            genList = new GenerateIEnumerableList(new DBContext());
            model.RoleList = genList.GetDBRoleList(model.RoleID);
            model.StatusList = genList.GetUserStatusList(model.Status);
            model.EmployeeList = genList.GetEmployeeList(model.EmployeeID);
            return PartialView("_AddEditUser", model);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult Post(User model)
        {
            String msg = "1";
            //model.IsApproved = 1;
            String host = HttpContext.Request.Url.Authority;

            if (!(host.StartsWith("http://") || host.StartsWith("https://")))
            {
                host = "http://" + host;
            }

            //if newly added
            if (model.UserID == 0)
            {
                msg = uDal.SaveUser(model);
                var newUser = uDal.FindUser(model.UserID);
                if (msg == "1")
                { 
                    MailMaster mm = new MailMaster(ConfigurationManager.AppSettings["SMTPServerIP"].ToString());
                    String mailBody = "Dear <strong>" + uDal.getEmployeeFullName(model.EmployeeID) + "</strong>, <br/><br/>" +
                                "Our team has granted you access rights for the MVC Template. <br/><br/>" +
                                "Please access the " + "<a href='" + host + "'>MVC Template</a> to start using the tool. <br/><br/>" +
                                "Kind regards, <br/>" +
                                "MVC Template Team";
                    mm.sendMailNotification(model.EmailAddress, "", "MVC Template User Access Request Granted", mailBody);
                }
            }
            else
            {
                msg = uDal.SaveUser(model);
            }

            return Json(new { Data = msg, ContentType = "" }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult RegisterUser(User model)
        {
            model.RoleID = 2;
            String msg = uDal.SaveUser(model);
            return Json(new { Data = msg, ContentType = "" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [CustomAuthorize]
        public string Delete(int id)
        {
            String msg = "1";
            User model = uDal.FindUser(id); ;
            if (model!=null)
            {
                msg = uDal.deleteUser(model);
            }

            return msg;
        }

      
    }
}