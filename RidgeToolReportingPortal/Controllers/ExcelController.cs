﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace RidgeToolReportingPortal.Controllers
{
    public class ExcelController : Controller
    {
        // GET: Excel
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ReadExcel()
        {
            Dictionary<int, Dictionary<int, string>> returnDict = new Dictionary<int, Dictionary<int, string>>();
            HttpPostedFileBase file = Request.Files[0];
            string filename = Path.GetFileName(Server.MapPath(file.FileName));

            string path = Server.MapPath("~/Uploads/Product/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string filePath = path + Path.GetFileName("ProductUploadSheet-" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss-ff") + Path.GetExtension(file.FileName));
            string extension = Path.GetExtension("ProductUploadSheet-" + DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss-ff") + Path.GetExtension(file.FileName));
            file.SaveAs(filePath);

            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePath);
            Excel.Worksheet xlWorkSheet = xlWorkBook.Sheets[1];
            Excel.Range xlRange = xlWorkSheet.UsedRange;
            Excel.Range last = xlWorkSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);

            int lastUsedRow = last.Row;
            int lastUsedCol = last.Column;

            for (int row = 1; row < lastUsedRow; row++)
            {
                var newDict = new Dictionary<int, string>();
                for (int col = 1; col < lastUsedCol; col++)
                {
                    if (xlRange.Cells[row, col] != null && xlRange.Cells[row, col].Value2 != null)
                    {
                        newDict[col] = xlRange.Cells[row, col].Value2.ToString();
                    }
                }
                returnDict[row] = newDict;
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorkSheet);
            //close and release
            xlWorkBook.Close();
            Marshal.ReleaseComObject(xlWorkBook);

            //quit and release
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);

            return Json(returnDict);
        }
    }
}