﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

using RidgeToolReportingPortalDAL;
using RidgeToolReportingPortalModel;


namespace RidgeToolReportingPortal.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeDAL empDal = new EmployeeDAL(new DBContext());
        ClsMyContext cls = new ClsMyContext(ConfigurationManager.ConnectionStrings["TemplateDB"].ConnectionString);
        GenerateIEnumerableList genList = null;
        public ActionResult Index()
        {
            return View();
        }
        [CustomAuthorize]
        public ActionResult AjaxList()
        {
            var allList = empDal.ListUser(false);
            var result = from c in allList.ToList()
                         select new[] {
                             c.Firstname + " " + c.Middlename + " " + c.Lastname,
                             c.Position != null? c.Position.PositionName : "" ,
                             c.Supervisor != null ? c.Supervisor.FullName : "" ,
                             String.Format("{0:MM-dd-yyyy}",  c.DateHired),
                             c.IsResigned.ToString(),
                             c.EmployeeID.ToString()
                         };
            return Json(new
            {
                aaData = result,
            },
            JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        public ActionResult AddEditEmployee(int id)
        {
            Employee model = null;
            if (id != 0)
            {
                model = empDal.findEmployee(id);
            }
            if (model == null)
            {
                model = new Employee();
                model.DateHired = DateTime.Now.Date;
            }
            genList = new GenerateIEnumerableList(new DBContext());
            model.PositionList = genList.GetDBPositionList(model.PositionID);
            model.SupervisorList = genList.GetSupervisorList(model.SupervisorID);
            return PartialView("_AddEditEmployee", model);
        }

        [HttpPost]
        [CustomAuthorize]
        public JsonResult Post(Employee model)
        {
            String msg = "1";
            String host = HttpContext.Request.Url.Authority;

            if (!(host.StartsWith("http://") || host.StartsWith("https://")))
            {
                host = "http://" + host;
            }
            //if newly added
            msg = empDal.SaveEmployee(model);

            return Json(new { Data = msg, ContentType = "" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [CustomAuthorize]
        public string Delete(int id)
        {
            String msg = "1";
            var model = empDal.findEmployee(id); ;
            if (model != null)
            {
                msg = empDal.deleteEmployee(model);
            }

            return msg;
        }

    }


}