﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Net;

using RidgeToolReportingPortalDAL;
using RidgeToolReportingPortalModel;

namespace RidgeToolReportingPortal.Controllers
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        DBContext db = null;
        public string currentController = String.Empty;
        public string currentAction = String.Empty;
        public string currentId = String.Empty;

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            string cont = currentController.ToLower();
            string contAction = currentAction.ToLower();
            string id = currentId;

            string fullPath = cont + "/" + contAction;

            IPrincipal user = httpContext.User;
            if (!user.Identity.IsAuthenticated)
            {
                //httpContext.Response.Redirect("Login");
                httpContext.Response.RedirectToRoute("Default", new { controller = "Login", action = "Index" });
            }
            else
            {
                var currentUser = ((Custom2IPrincipal)HttpContext.Current.User).UserId;

                if (user != null)
                {
                    return true;
                    //db = new DBContext();

                    //User currUser = db.User.Where(c => c.UserID == currentUser).FirstOrDefault();
                    //if (currUser.IsAdmin)
                    //{
                    //    return true;
                    //}
                    //else
                    //{
                    //    if (cont == "bom"
                    //        || cont == "distributor"
                    //        || cont == "compare"
                    //        || cont == "home"
                    //        || cont == "manufacturer"
                    //        || cont == "parametricsearch"
                    //        || cont == "partsummary"
                    //        || cont == "searchresults"
                    //        )
                    //    {
                    //        return true;
                    //    }
                    //    else if (cont == "parts" && (contAction == "viewpart" || contAction == "getparttaxonomyid"))
                    //    {
                    //        return true;
                    //    }
                    //    else if(cont == "manufacturer" || cont == "distributor" )
                    //    {
                    //        if (contAction != "post"
                    //            || contAction != "delete"
                    //            || contAction != "deletecontact"
                    //            || contAction != "togglestatus"
                    //            || contAction != "addeditmanufacturer"
                    //            || contAction != "addeditdistributor"
                    //            || contAction != "post"
                    //            )
                    //        {
                    //            return true;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        httpContext.Response.Redirect("Home");
                    //    }
                    //}
                }
            }
            return false;
            //----------------------------------------
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            currentController = (String)filterContext.RouteData.Values["controller"];
            currentAction = (String)filterContext.RouteData.Values["action"];
            currentId = (String)filterContext.RouteData.Values["id"];
            base.OnAuthorization(filterContext);      


            //logout if no cookie found
            //if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            //{
            //    HttpCookie sessionCokie = filterContext.HttpContext.Request.Cookies["_CurrentSessionId"];
            //    if (sessionCokie == null)
            //    {
            //        //var db = new DBContext();
            //        //ScUsers usr = db.ScUsers.Where(a => a.UserName == filterContext.HttpContext.User.Identity.Name).FirstOrDefault();
            //        //if (usr != null)
            //        //{
            //        //    usr.IsStatus = 0;
            //        //    usr.ModifiedBy = usr == null ? "" : filterContext.HttpContext.User.Identity.Name;
            //        //    usr.LastModified = DateTime.Now;
            //        //    usr.ModifiedSrc = 0;
            //        //    db.Entry(usr).State = System.Data.EntityState.Modified;
            //        //    db.SaveChanges();

            //        //    ScUserActivity ac = db.UserActivity.Where(a => a.Id == usr.SessionId).FirstOrDefault();
            //        //    if (ac != null)
            //        //    {
            //        //        ac.DateLogout = DateTime.Now;
            //        //        db.Entry(ac).State = System.Data.EntityState.Modified;
            //        //        db.SaveChanges();
            //        //    }
            //        //}
            //    }
            //}


        }
    }
}