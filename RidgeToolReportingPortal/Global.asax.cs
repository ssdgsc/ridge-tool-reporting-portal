﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


using System.Web.Script.Serialization;
using System.Web.Security;
using RidgeToolReportingPortalModel;

namespace RidgeToolReportingPortal
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                try
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    CustomPrincipalSerializeModel serializeModel = serializer.Deserialize<CustomPrincipalSerializeModel>(authTicket.UserData);

                    Custom2IPrincipal newUser = new Custom2IPrincipal(authTicket.Name);
                    newUser.UserId = serializeModel.UserID;
                    newUser.UserName = serializeModel.UserName;
                    newUser.RoleId = serializeModel.RoleId;

                    HttpContext.Current.User = newUser;
                }

                catch
                {
                    FormsAuthentication.SignOut();
                }
            }
        }
    }
}
