﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RidgeToolReportingPortalDAL;
using System.Web.Mvc;
using RidgeToolReportingPortalModel;
using System.IO;

using System.Web.Services;


namespace RidgeToolReportingPortalDAL
{
    public class GenerateIEnumerableList
    {
        DBContext db = null;
        public GenerateIEnumerableList(DBContext context)
        {
            db = context;
        }

        #region for DropDownList
        //generate SelectListItem from db
        public IEnumerable<SelectListItem> GetUserList(int? selectedVal)
        {
            var model = db.User.ToList();
            var list = from c in model
                       select new SelectListItem
                       {
                           Text = c.Username,
                           Value = c.UserID.ToString(),
                           Selected = c.UserID == selectedVal ? true : false
                       };
            return list;
        }
        //generate SelectListItem from db (role)
        public IEnumerable<SelectListItem> GetDBRoleList(int? selectedVal)
        {
            var model = db.Role.ToList();
            var list = from c in model
                       select new SelectListItem
                       {
                           Text = c.RoleName,
                           Value = c.RoleID.ToString(),
                           Selected = c.RoleID == selectedVal ? true : false
                       };
            return list;
        }
        public IEnumerable<SelectListItem> GetDBPositionList(int? selectedVal)
        {
            var model = db.Position.ToList();
            var list = from c in model
                       select new SelectListItem
                       {
                           Text = c.PositionName,
                           Value = c.PositionID.ToString(),
                           Selected = c.PositionID == selectedVal ? true : false
                       };
            return list;
        }

        //generate SelectListItem from static values
        public IEnumerable<SelectListItem> GetUserStatusList(int? selectedVal)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            SelectListItem item = new SelectListItem();

            string[] userType = { "Active", "Inactive" };

            for (int i = 1; i <= userType.Length; i++)
            {
                item = new SelectListItem();

                item.Value = i.ToString();
                item.Text = userType[i-1];
                item.Selected = i == selectedVal ? true : false;

                list.Add(item);
            }
            return list;
        }

        //generate SelectListItem from db
        public IEnumerable<SelectListItem> GetSupervisorList(int? selectedVal)
        {
            var model = db.Employee.ToList();
            var list = from c in model
                       where c.Position.PositionName != "Staff"
                       select new SelectListItem
                       {
                           Text = c.Firstname + " " + c.Lastname,
                           Value = c.EmployeeID.ToString(),
                           Selected = c.EmployeeID == selectedVal ? true : false
                       };
            return list;
        }

        public IEnumerable<SelectListItem> GetEmployeeList(int? selectedVal)
        {
           
            var model = db.Employee.ToList();
            IEnumerable<SelectListItem> list = null;

            if (selectedVal.Equals(0))
            {
                var listOfUser = new List<int>();
                foreach (var id in db.User.Select(param => param.EmployeeID))
                {
                    listOfUser.Add(id);
                }
                list = from c in model
                       where !listOfUser.Contains(c.EmployeeID)
                       select new SelectListItem
                       {
                           Text = c.FullName,
                           Value = c.EmployeeID.ToString(),
                           Selected = c.EmployeeID == selectedVal ? true : false
                       };
            }
            else
            {
                list = from c in model
                       where c.EmployeeID.Equals(selectedVal)
                       select new SelectListItem
                       {
                           Text = c.FullName,
                           Value = c.EmployeeID.ToString(),
                           Selected = c.EmployeeID == selectedVal ? true : false
                       };
            }
       
         
            return list;
        }

        #endregion


        public string deleteFile(String filePath, String fileName)
        {
            String msg = "1";
            if (System.IO.Directory.Exists(filePath))
            {
                var tempPath = Path.Combine(filePath, fileName);
                if (File.Exists(tempPath))
                {
                    File.Delete(tempPath);
                }
                else
                {
                    msg = "File does note exist. ";
                }
            }
            else
            {
                msg = "Directory does note exist. ";
            }
            return msg;
        }
    }
}