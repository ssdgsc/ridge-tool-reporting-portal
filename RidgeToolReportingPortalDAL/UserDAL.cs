﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;

using RidgeToolReportingPortalModel;

namespace RidgeToolReportingPortalDAL
{
    public class UserDAL
    {
		#region DAL using entity framework
		DBContext db = null;
        public UserDAL(DBContext context)
        {
            db = context;
        }
        public User FindUser(int id)
        {
            User model = null;
            try
            {
                model = db.User.Find(id);
            }
            catch { }
            return model;
        }
        public string deleteUser(User user)
        {
            string msg = "";
            try
            {
                db.User.Remove(user);
                db.SaveChanges();
                msg = "User is deleted";
            }
            catch(Exception ex)
            {
                msg = "Invalid user " + ex;
            }
            return msg;

        }
        public User FindUser(string username)
        {
            User model = null;
            try
            {

                model = db.User.Where(c => c.Username == username).FirstOrDefault();

                if (model == null)
                {
                    model = new User();
                }
            }
            catch (Exception ex)
            {
                model = new User();
            }
            return model;
        }
        public List<User> ListUser(int? status)
        {
            List<User> allList = null;
            try
            {
                if (status == 0)
                {
                    allList = db.User.OrderBy(c => c.Username).ToList();
                }
                else
                {
                    allList = db.User.Where(c => c.Status == status).OrderBy(c => c.Username).ToList();
                }
            }
            catch
            {
                allList = new List<User>();
            }
            return allList;
        }
        public string getEmployeeFullName(int id)
        {
            var getEmployeeFullname = db.Employee.FirstOrDefault(param => param.EmployeeID == id);
            return getEmployeeFullname.FullName.ToString();
        }
        public String SaveUser(User user)
        {
            String msg = "1";
         
            try
            {
                //business logic validation
                if (user.EmployeeID == 0)
                {
                    msg = "Please select employee";
                }
                else if (String.IsNullOrWhiteSpace(user.Username))
                {
                    msg = "Please enter <i>Username</i>.";
                }
                else if (String.IsNullOrWhiteSpace(user.EmailAddress))
                {
                    msg = "Please enter <i>Email Address</i>.";
                }
                else if (!(new EmailAddressAttribute().IsValid(user.EmailAddress)))
                {
                    msg = "Please enter valid <i>Email Address</i>.";
                }
                 else if (user.RoleID == 0)
                {
                    msg = "Please select <i>Role</i>.";
                }
                //db validations
                if (msg == "1")
                {
                    if (user.UserID == 0)
                    {
                        if (db.User.Where(c => c.Username == user.Username).FirstOrDefault() != null)
                        {
                            msg = "<i>Username</i> already exists! ";
                        }
                        else if (db.User.Where(param => param.EmployeeID == user.EmployeeID).FirstOrDefault() != null)
                        {
                            msg = "Employee is already exist.";
                        }
                        else if (db.User.Where(c => c.EmailAddress == user.EmailAddress).FirstOrDefault() != null)
                        {
                            msg = "<i>Email Address</i> already exists! ";
                        }
                        else
                        {
                            user.Status = 1;
                            db.User.Add(user);
                            db.SaveChanges();
                        }
                     
                    }
                    else
                    {
                        User model = db.User.Find(user.UserID);
                        if (model == null)
                        {
                            msg = "User does not exist! ";
                        }
                        else
                        {
                            model.Username = user.Username;
                            model.RoleID = user.RoleID;
                            model.Status = user.Status;
                            model.EmployeeID = user.EmployeeID;
                            model.EmailAddress = user.EmailAddress;

                            model.Updated = DateTime.Now;

                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return msg;
        }
        #endregion

    }
}
