﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Reflection;
using RidgeToolReportingPortalModel;


namespace RidgeToolReportingPortalDAL
{
    public class ClsMyContext : DataContext
    {
        public ClsMyContext()
            : base("TemplateDB")
        {
        }
        public ClsMyContext(String conString)
            : base(conString)
        {
        }

        [Function(Name = "sp_MVCListUser", IsComposable = false)]
        public ISingleResult<UserModel> ListUser([Parameter(Name = "@Typ", DbType = "nvarchar(3)")] String Typ)
        {
            IExecuteResult objResult = this.ExecuteMethodCall(this, (MethodInfo)(MethodInfo.GetCurrentMethod()), Typ);
            ISingleResult<UserModel> objresults = (ISingleResult<UserModel>)objResult.ReturnValue;
            return objresults;
        }
    }
    public class DBContext : DbContext
    {
        public DBContext()
            : base("TemplateDB")
        {
            Database.SetInitializer<DBContext>(null);
        }

        public DBContext(String conString)
            : base(conString)
        {
        }
        public DbSet<User> User { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Position> Position{ get; set; }
    }
}
