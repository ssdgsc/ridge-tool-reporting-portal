﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using RidgeToolReportingPortalModel;

namespace RidgeToolReportingPortalDAL
{
   public class EmployeeDAL
    {
        #region DAL using entity framework for employee
        DBContext db = null;
        public EmployeeDAL(DBContext context)
        {
            db = context;
        }
        public Employee findEmployee(int id)
        {
            Employee model = null;
            try
            {
                model = db.Employee.Find(id);
            }
            catch { }
            return model;
        }
        public string deleteEmployee(Employee employee)
        {
            string msg = "";
            try
            {
                db.Employee.Remove(employee);
                db.SaveChanges();
                msg = "User is deleted";
            }
            catch (Exception ex)
            {
                msg = "Invalid employee " + ex;
            }
            return msg;

        }
        public List<Employee> ListUser(bool isResign)
        {
            List<Employee> allList = null;
            try
            {
                if (isResign == false)
                {
                    allList = db.Employee.OrderBy(c => c.EmployeeID).ToList();
                }
                else
                {
                    allList = db.Employee.Where(c => c.IsResigned).OrderBy(c => c.EmployeeID).ToList();
                }
            }
            catch(Exception ex)
            {
                Console.Write(ex);
                allList = new List<Employee>();
            }
            return allList;
        }

        public String SaveEmployee(Employee employee)
        {
            String msg = "1";
            try
            {
                //business logic validation
                if (String.IsNullOrWhiteSpace(employee.Firstname))
                {
                    msg = "Please enter <i>First Name</i>.";
                }

                else if (String.IsNullOrWhiteSpace(employee.Middlename))
                {
                    msg = "Please enter <i>Last Name</i>.";
                }
                else if (String.IsNullOrWhiteSpace(employee.Lastname))
                {
                    msg = "Please enter <i>Username</i>.";
                }
                else if (employee.DateHired.ToString() == "1/1/0001 12:00:00 AM")
                {
                    msg = "Please enter <i>Date Hired</i>.";
                }
                else if(employee.SupervisorID == 0)
                {
                    employee.SupervisorID = -1;
                }
        
                //db validations
                if (msg == "1")
                {
                    if (employee.EmployeeID == 0)
                    {
                        employee.Created = DateTime.Now;
                        db.Employee.Add(employee);
                        db.SaveChanges();
                    }
                    else
                    {
                        var model = db.Employee.Find(employee.EmployeeID);
                        if (model == null)
                        {
                            msg = "Employee does not exist! ";
                        }
                        else
                        {
                            model.Firstname = employee.Firstname;
                            model.Middlename = employee.Middlename;
                            model.Lastname = employee.Lastname;
                            model.DateHired = employee.DateHired;
                            model.SupervisorID = employee.SupervisorID;
                            model.PositionID = employee.PositionID;
                            model.IsResigned = employee.IsResigned;

                            //model.UpdatedBy = //user
                            model.Updated = DateTime.Now;

                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return msg;
        }


        #endregion
    }
}
